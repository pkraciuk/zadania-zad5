#!/usr/bin/python
# -*- coding: utf-8 -*-

import cgi
import cgitb
cgitb.enable()
import os
import datetime
import time

now = time.time()
print os.environ
if os.environ.has_key('REMOTE_HOST'):
    host = os.environ['REMOTE_HOST']
else:
    host = 'unknown'
print "Content-Type: text/html"
print

body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Żądanie przyszło o %s.<br>
    </body>
</html>""" % (

        os.environ['SERVER_NAME'],
        os.environ['SERVER_ADDR'],
        os.environ['SERVER_PORT'],
        host,
        os.environ['REMOTE_ADDR'],
        os.environ['REMOTE_PORT'],
        os.environ['SCRIPT_NAME'],
        str(datetime.datetime.fromtimestamp(now)),
)

print body,