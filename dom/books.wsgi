#!/usr/bin/python
# -*- coding=utf-8 -*-

import bookdb
import urlparse
from cgi import escape

dictionary = {}
print ''
bodyup = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Homework: book list</title>
    </head>
    <body>
        <h1>This is list of the books in a database</h1>
        <ul>"""
bodydown="""
        </ul>
    </body>
</html>
"""

def application(environ, start_response):
    d = urlparse.parse_qs(environ.get('QUERY_STRING', 'Unset'))
    bookid = d.get('id', [''])

    idexists = False

    for title in bookdb.BookDB().titles():
        if title['id']==bookid[0]:
            idexists=True


    response_body="""
    <html>
        <head>
            <meta charset="utf-8" />"""

    if idexists:
        response_body+="""
                <title>Homework: book description</title>
            </head>
            <body>
                <h2>Description of selected book</h2>"""
        response_body+='Book title: <b>'+bookdb.BookDB().title_info(bookid[0])['title']+'</b></br>'
        response_body+='Book id: <b>'+bookid[0]+'</b></br>'
        response_body+='Book isbn: <b>'+bookdb.BookDB().title_info(bookid[0])['isbn']+'</b></br>'
        response_body+='Book publisher: <b>'+bookdb.BookDB().title_info(bookid[0])['publisher']+'</b></br>'
        response_body+='Book author: <b>'+bookdb.BookDB().title_info(bookid[0])['author']+'</b></br>'
        response_body+="""
            </body>
        </html>"""


    elif bookid[0]=='':
        response_body+="""
                <title>Homework: book list</title>
            </head>
            <body>
                <h2>List of the books in a database</h2>
                <ul>"""
        for title in bookdb.BookDB().titles():
            #response_body+='<li><a href="http://localhost:4444/?id='+title['id']+'">'+title['title']+'</a></li>'
            response_body+='<li><a href="http://194.29.175.240:31006/?id='+title['id']+'">'+title['title']+'</a></li>'
        response_body+="""
                </ul>
            </body>
        </html>"""
    else:
        response_body+="""
                <title>Homework: unknown id</title>
            </head>
            <body>
                <h2>Unknown id</h2>
                <p><b>The id you provided is not in our database</b></p>
            </body>
        </html>"""
    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('194.29.175.240',31006, application)
    #srv = make_server('localhost',4444, application)
    srv.serve_forever()