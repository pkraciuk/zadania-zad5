#!/usr/bin/python
# -*- coding=utf-8 -*-

import datetime
import time

dictionary = {}
print ''
body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Żądanie przyszło o %s.<br>
        <br>
        Ostatnia twoja wizyta miała miejsce %s.<br>
    </body>
</html>
"""

def application(environ, start_response):
    now = time.time()
    lastvisit = "never"
    if dictionary.has_key(environ.get('REMOTE_ADDR','Unset')):
        lastvisit = dictionary[environ.get('REMOTE_ADDR','Unset')]
    dictionary[environ.get('REMOTE_ADDR','Unset')]=str(datetime.datetime.fromtimestamp(now))
    for item in environ.items():
        print item
    response_body = body % (
         environ.get('SERVER_NAME', 'Unset'), # nazwa serwera
         str(environ.get('HTTP_HOST','Unset')).split(":")[0], # IP serwera
         environ.get('SERVER_PORT','Unset'), # port serwera
         environ.get('USERNAME','Unset'), # nazwa klienta
         environ.get('REMOTE_ADDR','Unset'), # IP klienta
         str(environ.get('SSH_CLIENT','Unset')).split(" ")[1], # port klienta
         environ.get('SCRIPT_NAME','Unset'), # nazwa skryptu
         str(datetime.datetime.fromtimestamp(now)), # bieżący czas
         lastvisit, # czas ostatniej wizyty
    )
    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('194.29.175.240',31006, application)
    #srv = make_server('localhost',4444, application)
    srv.serve_forever()